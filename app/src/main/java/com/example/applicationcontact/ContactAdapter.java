package com.example.applicationcontact;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class ContactAdapter extends RecyclerView.Adapter<ContactAdapter.ContactViewHolder>{

    private List<Contact> mContact;
    private Context context;
    public ContactAdapter(Context context){
        this.context = context;
    }

    public void setData(List<Contact> list) {
        this.mContact = list;
        notifyDataSetChanged();
    }
    @NonNull
    @Override
    public ContactAdapter.ContactViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_contact, parent, false);
        return new  ContactAdapter.ContactViewHolder(view);
    }


    @Override
    public void onBindViewHolder(@NonNull ContactAdapter.ContactViewHolder holder, int position) {
        Contact contact =mContact.get(position);
        if(contact == null){
            return;
        }
        holder.imgContact.setImageResource(contact.getImg());
        holder.tvphone.setText(contact.getPhone());
        holder.tvname.setText(contact.getName());
    }

    @Override
    public int getItemCount() {
        if(mContact != null)
            return mContact.size();
        return 0;
    }
    public class ContactViewHolder extends RecyclerView.ViewHolder{

        private ImageView imgContact;
        private TextView tvname;
        private TextView tvphone;
        private ConstraintLayout layoutitem;
        public ContactViewHolder(@NonNull View itemView) {
            super(itemView);
            layoutitem = itemView.findViewById(R.id.layoutitem);
            imgContact = itemView.findViewById(R.id.imgperson);
            tvname = itemView.findViewById(R.id.tvname);
            tvphone = itemView.findViewById(R.id.tvphone);
        }
    }
}
