package com.example.applicationcontact;

public class Contact {
    int img;
    String name;
    String phone;

    public Contact(int idimg, String name, String phone) {
        this.img = idimg;
        this.name = name;
        this.phone = phone;
    }

    public int getImg() {
        return img;
    }

    public void setImg(int img) {
        this.img = img;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }
}
