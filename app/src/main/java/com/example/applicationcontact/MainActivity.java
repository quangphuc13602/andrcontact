package com.example.applicationcontact;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private RecyclerView recyclerView;
    private ContactAdapter contactAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        recyclerView = findViewById(R.id.rcvContact);
        contactAdapter = new ContactAdapter(MainActivity.this);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(MainActivity.this, RecyclerView.VERTICAL, false);
        recyclerView.setLayoutManager(linearLayoutManager);

        contactAdapter.setData(getContact());
        recyclerView.setAdapter(contactAdapter);
    }
    private List<Contact> getContact(){
        List<Contact> contacts =new ArrayList<>();

        contacts.add(new Contact(R.drawable.heoava,"PuPeo","0378205310"));
        contacts.add(new Contact(R.drawable.heoava,"Me","0905856394"));
        contacts.add(new Contact(R.drawable.heoava,"Ba","0914747458"));
        contacts.add(new Contact(R.drawable.heoava,"Unknown","0777856311"));
        contacts.add(new Contact(R.drawable.heoava,"Helen","05113205884"));

        return contacts;
    }
}